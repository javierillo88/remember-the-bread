import pytest
from main import app


@pytest.fixture
def client():

    """ Get the test client for the Flask application """

    app.testing = True
    return app.test_client()


def test_index_success(client):

    """ Should get the HTML corresponding the webapp """

    response = client.get('/', follow_redirects=True)
    assert response.status_code == 200
    assert response.content_type == 'text/html; charset=utf-8'
    assert response.data.decode('utf-8').startswith('<!DOCTYPE html>')
